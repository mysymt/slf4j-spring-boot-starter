package wiki.xsx.core.log;

import org.slf4j.Logger;
import wiki.xsx.core.support.LogHandler;
import wiki.xsx.core.support.MethodInfo;

/**
 * 默认参数日志格式化实现
 * @author xsx
 * @date 2020/5/7
 * @since 1.8
 */
public class DefaultParamLogFormatter implements ParamLogFormatter {

    /**
     * 默认实现名称
     */
    public static final String DEFAULT = DefaultParamLogFormatter.class.getName();

    /**
     * 格式化
     * @param log 日志对象
     * @param level 日志级别
     * @param busName 业务名称
     * @param methodInfo 方法信息
     * @param args 参数列表
     * @param filterParamNames 参数过滤列表
     */
    @Override
    public void format(
            Logger log,
            Level level,
            String busName,
            MethodInfo methodInfo,
            Object[] args,
            String[] filterParamNames
    ) {
        LogHandler.print(log, level, LogHandler.getBeforeInfo(busName, methodInfo, args, filterParamNames));
    }
}
