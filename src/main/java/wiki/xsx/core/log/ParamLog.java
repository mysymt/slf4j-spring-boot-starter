package wiki.xsx.core.log;

import java.lang.annotation.*;

/**
 * 参数日志
 * @author xsx
 * @date 2019/6/17
 * @since 1.8
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ParamLog {
    /**
     * 业务名称
     * @return
     */
    String value();

    /**
     * 日志级别
     * @return
     */
    Level level() default Level.UNKNOWN;

    /**
     * 代码定位支持
     * @return
     */
    Position position() default Position.UNKNOWN;

    /**
     * 参数过滤
     * @return
     */
    String[] paramFilter() default {};

    /**
     * 参数格式化
     * @return
     */
    Class<? extends ParamLogFormatter> formatter() default DefaultParamLogFormatter.class;

    /**
     * 回调
     * @return
     */
    Class<? extends LogCallback> callback() default LogCallback.class;
}
